package com.edgenda.bnc.eventsmanager.service;

import com.edgenda.bnc.eventsmanager.service.model.Event;
import com.edgenda.bnc.eventsmanager.service.repository.EventRepository;
import static org.mockito.Mockito.*;

import com.edgenda.bnc.eventsmanager.service.service.EventService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EventServiceTest {

@InjectMocks
private EventService service;

@Mock
private EventRepository eventRepo;

@Before
public void init()
{
    MockitoAnnotations.initMocks(EventServiceTest.this);
}

    @Test
    public void getEventTest() {

        List<Event> events = new ArrayList<Event>();
        events.add(new Event());
        events.add(new Event());
        when(eventRepo.findAll()).thenReturn(events);
        List<Event> retrievedEvents = service.getEvents();
        assertNotNull(retrievedEvents);
        assertEquals(2, retrievedEvents.size());
    }
}