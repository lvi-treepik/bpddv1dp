//Hello Test
package com.edgenda.bnc.eventsmanager.service.rest;

import com.edgenda.bnc.eventsmanager.service.model.Event;
import com.edgenda.bnc.eventsmanager.service.model.Guest;
import com.edgenda.bnc.eventsmanager.service.service.GuestService;
import com.edgenda.bnc.eventsmanager.service.service.exception.UnknownGuestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/guests")
public class GuestController {

    private final GuestService guestService;

    @Autowired
    public GuestController(GuestService guestService) {
        this.guestService = guestService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Guest> getGuests() {
        return guestService.getGuests();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getGuest(@PathVariable Long id) {
        try {
            return new ResponseEntity<>(guestService.getGuest(id), HttpStatus.OK);
        } catch (UnknownGuestException e) {
            return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Guest createGuest(@RequestBody Guest guest) {
        return guestService.createGuest(guest);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public void updateGuest(@PathVariable Long id, @RequestBody Guest guest) {
        guestService.updateGuest(
                new Guest(
                        id,
                        guest.getFirstName(),
                        guest.getLastName(),
                        guest.getEmail(),
                        guest.getEvents()
                )
        );
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void deleteGuest(@PathVariable Long id) {
        guestService.deleteGuest(id);
    }




    @RequestMapping(path = "/{id}/events", method = RequestMethod.GET)
    public List<Event> getEventsWithGuest(@PathVariable Long id) {
        return guestService.getEventsWithGuests(id);
    }
}
