package com.edgenda.bnc.eventsmanager.service.rest;
//Hello//
import com.edgenda.bnc.eventsmanager.service.model.Event;
import com.edgenda.bnc.eventsmanager.service.model.Guest;
import com.edgenda.bnc.eventsmanager.service.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/events")
public class EventController {

    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Event getEvent(@PathVariable Long id) {
        return eventService.getEvent(id);
    }

    @RequestMapping( method = RequestMethod.GET)
    public List<Event> getEventsByDate(@RequestParam(name = "evtdate", required = false) String evtdate) {
        if(StringUtils.isEmpty(evtdate))
        {
            return eventService.getEvents();
        }
        return eventService.getEventsByDate(evtdate);
    }



    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Event createEvent(@RequestBody Event event) {
        return eventService.createEvent(event);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public void updateEvent(@PathVariable Long id, @RequestBody Event event) {
        eventService.updateEvent(
                new Event(
                        id,
                        event.getName(),
                        event.getDescription(),
                        event.getEvtdate(),
                        event.getGuests()
                )
        );
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void deleteEvent(@PathVariable Long id) {
        eventService.deleteEvent(id);
    }

    @RequestMapping(path = "/{id}/guests", method = RequestMethod.GET)
    public List<Guest> getEventGuests(@PathVariable Long id) {
        return eventService.getEventGuests(id);
    }

}
