INSERT INTO GUEST(ID, FIRST_NAME, LAST_NAME, EMAIL) VALUES (1, 'Brice', 'Argenson', 'bargenson@edgenda.com');
INSERT INTO GUEST(ID, FIRST_NAME, LAST_NAME, EMAIL) VALUES (2, 'Gregoire', 'Weber', 'gweber@cleverage.com');
INSERT INTO GUEST(ID, FIRST_NAME, LAST_NAME, EMAIL) VALUES (3, 'John', 'Doe', 'jdoe@bnc.com');

INSERT INTO EVENT(ID, NAME, DESCRIPTION,EVTDATE) VALUES (1, 'Spring', 'The Spring Framework is an application framework and inversion of control container for the Java platform. ', '2019-03-20');
INSERT INTO EVENT(ID, NAME, DESCRIPTION,EVTDATE) VALUES (2, 'REST API', 'Representational State Transfer (REST) is an architectural style that defines a set of constraints to be used for creating web services.', '2019-03-24');
INSERT INTO EVENT(ID, NAME, DESCRIPTION,EVTDATE) VALUES (3, 'Jenkins', 'Jenkins is an open source automation server written in Java.', '2019-03-28');

INSERT INTO EVENTS_GUESTS(GUEST_ID, EVENT_ID) VALUES (1, 1);
INSERT INTO EVENTS_GUESTS(GUEST_ID, EVENT_ID) VALUES (2, 1);
INSERT INTO EVENTS_GUESTS(GUEST_ID, EVENT_ID) VALUES (3, 1);
INSERT INTO EVENTS_GUESTS(GUEST_ID, EVENT_ID) VALUES (2, 2);
INSERT INTO EVENTS_GUESTS(GUEST_ID, EVENT_ID) VALUES (3, 2);
INSERT INTO EVENTS_GUESTS(GUEST_ID, EVENT_ID) VALUES (3, 3);