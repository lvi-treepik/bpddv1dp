FROM openjdk:8-jdk-alpine
#VOLUME /volumes/jenkins_volume/_data/workspace/Guests-Manager\ pipeline\@2/target xxxx
COPY ./guests-manager.jar guests-manager.jar 
ENTRYPOINT ["java","-jar","/guests-manager.jar"]
EXPOSE 8081